#include <fstream>
#include <iostream>
#include <vector>
#include <cassert>
#include <cstdint>

std::vector<std::uint8_t> load_data(const std::string& filename)
{
	std::ifstream fin(filename);
	fin.seekg(0, std::ios::end);
	std::vector<std::uint8_t> data(fin.tellg());
	fin.seekg(0, std::ios::beg);
	fin.read((char*)data.data(), data.size());
	return data;
}

char tohex(unsigned v) {
	v &= 0xF;

	if (v < 10)
		return v + '0';
	else
		return 'A' - 10 + v;
}

const std::string type_names[] = {
	"misc",
	"timestamp",
	"reg",
	"wave_start",
	"wave_alloc",
	"reg_cspriv",
	"wave_end",
	"event",
	"event_cs",
	"event_gfx1"
};


enum class TokenType {
	misc = 0,
	timestamp = 1,
	reg = 2,
	wave_start = 3,
	wave_alloc = 4,
	reg_cspriv = 5,
	wave_end = 6,
	event = 7,
	event_cs = 8,
	event_gfx1 = 9,
};

const std::string get_type_name(TokenType type) {
	unsigned v = static_cast<unsigned>(type);
	if (v > 9)
		return "unknown";
	return type_names[v];
}

struct Token {
	TokenType type;
	std::uint64_t timestamp;

	/* valid for wave_start */
	std::uint8_t pipe_index_1;
	std::uint8_t pipe_index_2;

	std::uint8_t se;
	std::uint8_t sh;
	std::uint8_t cu;
	std::uint8_t simd;
	std::uint8_t wave;
};
unsigned rgpSqttGetTokenSizeFromType(unsigned type)
{
	static unsigned sizes[] = {2, 8, 8, 4, 2, 6, 2, 2, 2, 2, 2, 8, 6, 4, 8, 6};
	return sizes[type & 0xF];
}


class TokenParser {
public:
	TokenParser(const std::uint8_t *data, unsigned length) : data_(data), offset_(0), end_(length), timestamp_(0), base_timestamp_(0) {
		assert(length % 2 == 0);
	}

	bool parse_next(Token& token) {
		do {
			auto offset = offset_;
			if (end_ - offset_ < 2) {
				return false;
			}
			token = {};
			token.type = static_cast<TokenType>(data_[offset_]);
			if (end_ - offset_ < rgpSqttGetTokenSizeFromType(static_cast<unsigned>(token.type))) {
				return false;
			}

			offset_ += rgpSqttGetTokenSizeFromType(static_cast<unsigned>(token.type));

			switch(token.type) {
			case TokenType::misc:
				timestamp_ += (*reinterpret_cast<const std::uint16_t*>(data_ + offset) >> 2) & 0x3fc;
				break;
			case TokenType::timestamp: {
				std::uint64_t timestamp =
					((std::uint64_t)data_[offset + 1] << 0x10) |
					((std::uint64_t)data_[offset + 2] << 0x00) |
					((std::uint64_t)data_[offset + 3] << 0x08) |
					((std::uint64_t)data_[offset + 5] << 0x18) |
					((std::uint64_t)data_[offset + 6] << 0x20) |
					((std::uint64_t)data_[offset + 7] << 0x28);
				std::cout << std::hex << timestamp << std::dec << "\n";
				if (!base_timestamp_)
					base_timestamp_ = timestamp;
				timestamp_ = timestamp;
				break;
			}
			case TokenType::wave_start: {
				token.timestamp = timestamp_ - base_timestamp_;
				if(data_[offset + 3] & 0x10) {
					token.pipe_index_1 = 0;
					token.pipe_index_2 = (data_[offset + 2] >> 3) & 1;
				} else {
					token.pipe_index_1 = data_[offset + 2] & 7;
					token.pipe_index_2 = ((data_[offset + 2] >> 2) & 3) + 1;
				}
				token.se = 0 /* unknown */;
				token.sh = (data_[offset] >> 5) & 1;
				token.cu = ((data_[offset] >> 6) & 3) |
				           ((data_[offset + 1] << 2) & 0xc);
				token.simd = (data_[offset + 1] >> 6) & 3;
				token.wave = (data_[offset + 1] >> 2) & 0xf;
				
				return true;
			}
			case TokenType::wave_end: {
				token.timestamp = timestamp_ - base_timestamp_;
				token.se = 0 /* unknown */;
				token.sh = (data_[offset] >> 5) & 1;
				token.cu = ((data_[offset] >> 6) & 3) |
				           ((data_[offset + 1] << 2) & 0xc);
				token.simd = (data_[offset + 1] >> 6) & 3;
				token.wave = (data_[offset + 1] >> 2) & 0xf;
				
				return true;
			}
			}
		} while(true);
	}
private:
	const std::uint8_t *data_;
	unsigned offset_;
	unsigned end_;

	uint64_t timestamp_;
	uint64_t base_timestamp_;
};
int main(int argc, char *argv[]) {
	if (argc != 2) {
		std::cerr << "Need the dump filename\n";
		return 1;
	}
	auto data = load_data(argv[1]);
	while(!data.empty() && data.back() == 0)
		data.pop_back();

	if (data.size() & 1)
		data.push_back(0);

	std::cout << data.size() << "\n";
	
	TokenParser parser((const std::uint8_t*)data.data(), data.size());
	Token token;
	while(parser.parse_next(token)) {
		std::cout << "token " << token.timestamp << " \"" << get_type_name(token.type) << "\": ";
		switch(token.type) {
		case TokenType::wave_start:
		case TokenType::wave_end:
			std::cout << "se: " << (unsigned)token.se << " sh: " << (unsigned)token.sh << " cu: " << (unsigned)token.cu << " simd: " << (unsigned)token.simd << " wave: " << (unsigned)token.wave;
			break;
		}
		std::cout << "\n";
	}
#if 0
	std::vector<std::uint16_t> data16(data.size() / 2);
	for (unsigned i = 0; i < data16.size(); ++i) {
		data16[i] = data[2*i] | ((std::uint16_t)data[2*i+1] << 8);
	}
	
	unsigned index = 0;
	for (; index < data16.size();) {
		if (data16[index] == 0 || data16[index] == 0x0010) {
			++index;
			continue;
		}
		unsigned type = data16[index] & 15;
		unsigned size = (rgpSqttGetTokenSizeFromType(type) / 2);
		unsigned remain = data16.size() - index;
		bool print = true;

		switch(type) {
		case 2: {
			unsigned header = data16[index];
			uint32_t value =  ((uint32_t)data16[index+3] << 16) | data16[index+2];

			std::string const names[] = {"event", "draw", "dispatch", "userdata", "marker", "gfxdec", "shdec", "other"};
			std::cout << "reg " << ((header >> 10) & 7)<< " " << names[(header >> 10) & 7]
			          << " pipe: " << ((header >> 5) & 3) << " me: " << ((header >> 7) & 3)
				  << " dropped: " << ((header >> 9) & 1) << " priv: " << ((header >> 14) & 1)
				  << " op: " << ((header >> 15) & 1) << " reg: " << std::hex << data16[index+1] << " " << (4 * (uint32_t)data16[index+1]) << std::dec << ": " << std::hex << value << std::dec;
			print = false;
			break;
		}
		case 3: {
			uint32_t header = ((uint32_t)data16[index+1] << 16) | data16[index];
			/*unsigned unk2c, unk30, unk34;
			
			if (full_header & 0x100000) { // is GFX queue?
				unk2c = (full_header >> 16) & 7; // stage?
				unk30 = 0;
				unk34 = (full_header >> 19) & 1;
			} else {
				unk2c = 6; // stage?
				unk30 = (full_header >> 16) & 7; // compute ring?
				unk34 = ((full_header >> 18) & 3) + 1;
			}
			if (full_header >> 29)
				std::cout << "warn: wave_start [29:31] used." << std::endl;
			std::cout << "wave_start unk1c: " << ((header >> 5) & 1) << " cu: " << ((header >> 6) & 0xf)
			          << " simd: " << ((header >> 14) & 3) << " wave: " << ((header >> 10) & 0xf)
				  << " unk2c: " << unk2c << " unk30: " << unk30 << " " << "unk34: " << unk34
				  << " unk38: " << ((full_header >> 22) & 0x7f) << " flag: " << ((full_header & 0x100000) ? 1 : 0);*/
			std::cout << "wave_start sh: " << ((header >> 5) & 1) << " cu: " << ((header >> 6) & 0xf)
			          << " wave: " << ((header >> 10) & 0xf) << " simd: " << ((header >> 14) & 3)
				  << " dispatcher: " << ((header >> 16) & 0x1f) << " vs_no_alloc " << ((header >> 21) & 1)
				  << " count: " << ((header >> 22) & 0x7f) << " tg_id: " << ((header >> 29) & 7);
			  print = false;
			break;
		}
		case 6: {
			unsigned header = data16[index];
			std::cout << "wave_end   unk1c: " << ((header >> 5) & 1) << " cu: " << ((header >> 6) & 0xf)
			          << " simd: " << ((header >> 14) & 3) << " wave: " << ((header >> 10) & 0xf);
			  print = false;
			break;
		}
		case 7: {
			std::cout << "event";
			break;
		}
		case 8: {
			std::cout << "event_cs";
			break;
		}
		case 9: {
			std::cout << "event_gfx";
			break;
		}
		default:
			std::cout << type;
		}

		size = std::min(size, remain);

		if (print) {
			std::cout << ":";
			for (unsigned j = 0; j < size; ++j) {
				unsigned e = data16[index + j];
				std::cout << " " << tohex(e >> 4) << tohex(e) << " " << tohex(e >> 12) << tohex(e >> 8);
			}
		}
		std::cout << "\n";
		index += size;
	}
	/*for(auto e : data16) {
		if ((e & 0xFF) == 0x44 || (e & 0xFF) == 0x06)
			std::cout << "\n";
		else
			std::cout << " ";

		std::cout << tohex(e >> 4) << tohex(e) << " " << tohex(e >> 12) << tohex(e >> 8);
	}*/
	std::cout << "\n";
#endif
	return 0;
}

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <iostream>
#include <string>
#include <fstream>
#include <cstdint>
#include <vector>
#include <map>
#include <cstring>

#define SQTT_FILE_MAGIC_NUMBER 0x50303042
#define SQTT_FILE_VERSION_MAJOR 1
#define SQTT_FILE_VERSION_MINOR 4

#define SQTT_GPU_NAME_MAX_SIZE 256
#define SQTT_MAX_NUM_SE 32
#define SQTT_SA_PER_SE 2

enum sqtt_version {
	SQTT_VERSION_NONE = 0x0,
	SQTT_VERSION_1_0  = 0x1,
	SQTT_VERSION_1_1  = 0x2,
	SQTT_VERSION_2_0  = 0x3, /* GFX6 */
	SQTT_VERSION_2_1  = 0x4, /* GFX7 */
	SQTT_VERSION_2_2  = 0x5, /* GFX8 */
	SQTT_VERSION_2_3  = 0x6, /* GFX9 */
	SQTT_VERSION_2_4  = 0x7  /* GFX10 */
};

/**
 * SQTT chunks.
 */
enum sqtt_file_chunk_type {
	SQTT_FILE_CHUNK_TYPE_ASIC_INFO = 0,
	SQTT_FILE_CHUNK_TYPE_SQTT_DESC = 1,
	SQTT_FILE_CHUNK_TYPE_SQTT_DATA = 2,
	SQTT_FILE_CHUNK_TYPE_API_INFO = 3,
	SQTT_FILE_CHUNK_TYPE_ISA_DATABASE = 4,
	SQTT_FILE_CHUNK_TYPE_QUEUE_EVENT_TIMINGS = 5,
	SQTT_FILE_CHUNK_TYPE_CLOCK_CALIBRATION = 6,
	SQTT_FILE_CHUNK_TYPE_CPU_INFO = 7,
	SQTT_FILE_CHUNK_TYPE_SPM_DB,
	SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_DATABASE,
	SQTT_FILE_CHUNK_TYPE_CODE_OBJECT_LOADER_EVENTS,
	SQTT_FILE_CHUNK_TYPE_PSO_CORRELATION,
	SQTT_FILE_CHUNK_TYPE_INSTRUMENTATION_TABLE,
	SQTT_FILE_CHUNK_TYPE_COUNT
};

struct sqtt_file_chunk_id {
	enum sqtt_file_chunk_type type : 8;
	int32_t index : 8;
	int32_t reserved : 16;
};

struct sqtt_file_chunk_header {
	struct sqtt_file_chunk_id chunk_id;
	uint16_t minor_version;
	uint16_t major_version;
	int32_t size_in_bytes;
	int32_t padding;
};

/**
 * SQTT file header.
 */
struct sqtt_file_header_flags {
	union {
		struct {
			int32_t is_semaphore_queue_timing_etw : 1;
			int32_t no_queue_semaphore_timestamps : 1;
			int32_t reserved : 30;
		};

		uint32_t value;
	};
};

struct sqtt_file_header {
	uint32_t magic_number;
	uint32_t version_major;
	uint32_t version_minor;
	struct sqtt_file_header_flags flags;
	int32_t chunk_offset;
	int32_t second;
	int32_t minute;
	int32_t hour;
	int32_t day_in_month;
	int32_t month;
	int32_t year;
	int32_t day_in_week;
	int32_t day_in_year;
	int32_t is_daylight_savings;
};

static_assert(sizeof(struct sqtt_file_header) == 56,
	      "sqtt_file_header doesn't match RGP spec");

struct sqtt_file_chunk_sqtt_data {
	struct sqtt_file_chunk_header header;
	int32_t offset; /* in bytes */
	int32_t size; /* in bytes */
};

std::vector<std::uint8_t> load_data(const std::string& filename)
{
	std::ifstream fin(filename);
	fin.seekg(0, std::ios::end);
	std::vector<std::uint8_t> data(fin.tellg());
	fin.seekg(0, std::ios::beg);
	fin.read((char*)data.data(), data.size());
	return data;
}

unsigned rgpSqttGetTokenSizeFromType(unsigned type)
{
	static unsigned sizes[] = {2, 8, 8, 4, 2, 6, 2, 2, 2, 2, 2, 8, 6, 4, 8, 6};
	return sizes[type & 0xF];
}

void process_sqtt(unsigned se_index, const std::uint8_t *data, std::size_t size)
{
	std::vector<std::uint16_t> data16(size/2);
	for (unsigned i = 0; i < data16.size(); ++i) {
		data16[i] = data[2*i] | ((std::uint16_t)data[2*i+1] << 8);
	}

	std::cout << "Processing SQTT for SE " << se_index << "\n";

	unsigned event_count = 0;
	unsigned event_cs_count = 0;
	std::uint64_t wave_start_count = 0;
	std::uint64_t wave_end_count = 0;
	std::map<std::uint16_t, std::uint64_t> reg_count;
	unsigned wave_start = 0;
	unsigned wave_end = 0;
	unsigned index = 0;
	std::map<std::uint32_t, std::uint32_t> ctx_regs;
	for (; index < data16.size();) {
		if (data16[index] == 0 || data16[index] == 0x0010) {
			++index;
			continue;
		}
		unsigned type = data16[index] & 15;
		unsigned size = (rgpSqttGetTokenSizeFromType(type) / 2);
		unsigned remain = data16.size() - index;
		bool print = true;

		switch(type) {
		case 2: {
			unsigned header = data16[index];
			uint32_t reg = data16[index+1];
			uint32_t value =  ((uint32_t)data16[index+3] << 16) | data16[index+2];
			unsigned rtype = (header >> 10) & 7;
			// gfxdec is about setting context registers, though there are some phantom writes here ...
			if (rtype == 5) {
				unsigned r = (reg * 4) & 0xF8FFF;
				std::cout << "  gfxdec " << std::hex << r << " (ctx: " << ((reg >> 10) & 7) << "): " << value << " prev: " << ctx_regs[r] << std::dec << "\n";
				ctx_regs[r] = value;
				break;
			} else if (rtype == 1) {
				unsigned ctx = (reg & 0x1C00) / 0x400;
				unsigned r = reg & 0xE3FF;
				std::cout << "  draw " << std::hex << r << " (ctx: " << ctx << "): " << value << std::dec << "\n";
				break;
			}

			// shdec is a plain shader register write
			// draw happens exactly once per draw call. The HW ctx is included in the draw reg
			// dispatch happens exactly once per compute dispatch
			std::string const names[] = {"event", "draw", "dispatch", "userdata", "marker", "gfxdec", "shdec", "other"};
			std::cout << "  " << names[rtype] << ": " << std::hex << data16[index + 1] << "(" << (data16[index + 1] * 4) << ") = " << value << std::dec << "\n";
			++reg_count[data16[index + 1]];
			
			if (data16[index + 1] >= 0xc080 && data16[index + 1] <= 0xc084) {
				std::cout << "  " << std::hex << data16[index + 1] << std::dec << " " << wave_start << " " << wave_end << "\n";
				wave_start = wave_end = 0;
			}
			break;
		}
		case 3: {
			++wave_start_count;
			++wave_start;
			std::cout << "    start wave\n";
			break;
		}
		case 6: {
			++wave_end_count;
			++wave_end;
			std::cout << "    end wave\n";
			break;
		}
		case 7: {
			std::cout << "EVT " << std::hex << data16[index] << std::dec << "\n";
			++event_count;
			break;
		}
		case 8: {
			std::cout << "EVT_CS " << std::hex << data16[index] << std::dec << "\n";
			++event_cs_count;
			break;
		}
		}

		size = std::min(size, remain);
		index += size;
	}

	std::cout << "    waves started: " << wave_start_count << "\n";
	std::cout << "    waves ended: " << wave_end_count << "\n";
	std::cout << "    events: " << event_count << "\n";
	std::cout << "    events_cs: " << event_cs_count << "\n";
	std::cout << "\n";
	for (auto e : reg_count)
		std::cout << "    " << std::hex << e.first << ": " << std::dec << e.second << "\n";
}

int main(int argc, char *argv[])
{
	auto data = load_data(argv[1]);

	unsigned offset = 56;
	unsigned idx = 0;
	while(offset + sizeof(sqtt_file_chunk_header) <= data.size()) {
		struct sqtt_file_chunk_header header;
		std::memcpy(&header, data.data() + offset, sizeof(header));
		if (header.size_in_bytes == 0) {
			std::cerr << " size in bytes = 0\n";
			return 1;
		}
		if (header.size_in_bytes > data.size() - offset) {
			std::cerr << "overflow " << offset << " " << header.size_in_bytes << " " << data.size() << "\n";
			return 1;
		}

		std::cout << "chunk of type " << header.chunk_id.type << " " << header.chunk_id.index << "  "<< header.size_in_bytes << " " << offset << "\n";
		if (header.chunk_id.type == SQTT_FILE_CHUNK_TYPE_SQTT_DATA) {
			struct sqtt_file_chunk_sqtt_data sqtt_header;
			std::memcpy(&sqtt_header, data.data() + offset, sizeof(sqtt_header));
			//std::cout << sqtt_header.offset << " " << sqtt_header.size << "\n";
			process_sqtt(idx++, data.data() + sqtt_header.offset, sqtt_header.size);
		}
		offset += header.size_in_bytes;
	}
  
}

